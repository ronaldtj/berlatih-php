<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ubah Huruf</title>
</head>
<body>
    <h1>Ubah Huruf</h1>
    <?php 

        echo "<h3> Soal No 1 </h3>";
    
        // Code function di sini
       

        function ubah_huruf($string){
        //kode di sini
            echo $string . " : ";
            for ($i=0; $i <= (strlen($string)-1) ;$i++){
                $cha = $string[$i];
                $next_cha = ++$cha; 
                //The following if condition prevent you to go beyond 'z' or 'Z' and will reset to 'a' or 'A'.
                if (strlen($next_cha) > 1) 
                {
                    $next_cha = $next_cha[0];
                }
                echo $next_cha;
            }
            echo "<br>";
        }

        // TEST CASES
        echo ubah_huruf('wow'); // xpx
        echo ubah_huruf('developer'); // efwfmpqfs
        echo ubah_huruf('laravel'); // mbsbwfm
        echo ubah_huruf('keren'); // lfsfo
        echo ubah_huruf('semangat'); // tfnbohbu
                    
    ?>
</body>
</html>